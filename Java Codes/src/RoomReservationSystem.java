import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class RoomReservationSystem {

    static final String URL = "jdbc:mysql://localhost:3306/RoomReservation";
    static final String USERNAME = System.getenv("DBMS_Username");//You can use your own mysql username for this field
    static final String PASSWORD = System.getenv("DBMS_Password");//You can use your own mysql password for this field
    private static Connection connection = null;
    private static final Scanner scanner = new Scanner(System.in);
    private static final String[] menuHolderArray = new String[]{"", "1. Create Room", "2. Remove Room", "3. List all Rooms in a Building With Their Vacancy Information"
            , "4. Get Currently Occupied Rooms", "5. Search For a Room", "6. Get Reservation History of a Room",
            "7. Get Reservation History of an Instructor", "8. Reserve a Room", "9. Leave a Room"};


    public static void establishConnection() {
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            System.out.println("An error occurred while establishing connection with the database");
        }
    }

    public static void closeConnection() {
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            System.out.println("An error occurred while establishing connection with the database");
        }
    }

    public static int getUserCapacity() {
        int capacity;
        String tempInput = scanner.nextLine();
        while (true) {
            try {
                capacity = Integer.parseInt(tempInput);
                if (capacity >= 0) {
                    return capacity;
                } else {
                    System.out.println("Please enter a proper integer value(>=0) capacity value");
                    System.out.print("capacity >: ");
                    tempInput = scanner.nextLine();
                }
            } catch (Exception e) {
                System.out.println("Please enter a proper integer value(>=0) capacity value");
                System.out.print("capacity >: ");
                tempInput = scanner.nextLine();
            }
        }
    }

    public static String isStringProperController() {
        String input = scanner.nextLine();
        while (true) {
            int howManyBlanksInIt = 0;
            for (int letters = 0; letters < input.length(); letters++) {
                if (input.charAt(letters) == ' ') {
                    howManyBlanksInIt++;
                }
            }
            if (howManyBlanksInIt != input.length() && !input.equals("")) {
                return input;
            } else {
                System.out.print("Please enter a proper value for this field!" + "\n" + "room_name >: ");
                input = scanner.nextLine();
            }
        }
    }


    public static void openingScreen(String[] menuHolderArray) {
        for (int elements = 1; elements < menuHolderArray.length; elements++) {
            System.out.println(menuHolderArray[elements]);
        }
    }

    public static void navigation(String[] menuHolderArray) {
        try {
            System.out.print("Please Select an Operation" + "\n" + "select >: ");
            Scanner mainMenuInput = new Scanner(System.in);
            int menuSelection = mainMenuInput.nextInt();
            if (menuSelection < 1 || menuSelection > 9) {
                System.out.println("Please enter a value between 1-9 inclusively");
                while (true) {
                    menuSelection = mainMenuInput.nextInt();
                    if (menuSelection > 0 && menuSelection < 10) {
                        break;
                    } else {
                        System.out.println("Please enter a value between 1-9 inclusively");
                    }
                }
            }
            //switch (menuSelection) {
            switch (menuSelection) {
                case 1:
                    System.out.println(menuHolderArray[menuSelection]);
                    CreateRoom();
                    openingScreen(menuHolderArray);
                    navigation(menuHolderArray);
                case 2:
                    removeRoom();
                    openingScreen(menuHolderArray);
                    navigation(menuHolderArray);
                case 3:
                    listWithVacancy();
                    openingScreen(menuHolderArray);
                    navigation(menuHolderArray);
                case 4:
                    getOccupiedRooms();
                    openingScreen(menuHolderArray);
                    navigation(menuHolderArray);
                case 5:
                    searchForRoom();
                    openingScreen(menuHolderArray);
                    navigation(menuHolderArray);
                case 6:
                    getReservationHistoryOfRoom();
                    openingScreen(menuHolderArray);
                    navigation(menuHolderArray);
                case 7:
                    getReservationHistoryOfInstructor();
                    openingScreen(menuHolderArray);
                    navigation(menuHolderArray);
                case 8:
                    reserveRoom();
                    openingScreen(menuHolderArray);
                    navigation(menuHolderArray);
                case 9:
                    leaveRoom();
                    openingScreen(menuHolderArray);
                    navigation(menuHolderArray);
            }
        } catch (Exception e) {
            System.out.println("Please enter a value between 1-9 inclusively");
            openingScreen(menuHolderArray);
            navigation(menuHolderArray);
        }
    }


    public static void CreateRoom() {
        System.out.print("room_name >: ");
        String room_name = isStringProperController();
        System.out.println("> OK");

        System.out.print("building_id >: ");
        String building_id = isStringProperController();
        System.out.println("> OK");

        System.out.print("capacity >: ");
        int capacity = getUserCapacity();
        System.out.println("> OK");

        System.out.print("property_string >: ");
        String property_string = isStringProperController();

        try {
            establishConnection();
            Statement getBuildingsandRoomsStatement = connection.createStatement();
            Statement createRoomStatement = connection.createStatement();
            String getBuildingsandRooms = "SELECT room_name, building_id FROM Room";
            ResultSet buildingsAndRooms = getBuildingsandRoomsStatement.executeQuery(getBuildingsandRooms);
            boolean doesExist = false;
            while (buildingsAndRooms.next()) {
                if (buildingsAndRooms.getString("room_name").equals(room_name) && buildingsAndRooms.getInt("building_id") == Integer.parseInt(building_id)) {
                    doesExist = true;
                }
            }
            if (doesExist) {
                System.out.println("-> You can't add this room because there is a room with the same name in this building.");
            } else {
                String createRoomQuery = "INSERT INTO RoomReservation.Room(ROOM_NAME, BUILDING_ID, CAPACITY, PROPERTY_STRING) VALUES('" + room_name + "','" + building_id + "','" + capacity + "','" + property_string + "')";
                createRoomStatement.executeUpdate(createRoomQuery);
                System.out.println("> OK");
                System.out.println("-> Room created successfully.");
            }
            closeConnection();
        } catch (SQLIntegrityConstraintViolationException e) {
            System.out.println("-> There is no such building!");
        } catch (Exception e) {

        }
    }

    public static void removeRoom() {
        System.out.print("-> " + "Please enter the ID of the room that you want to remove" + "\n" + "room_id >: ");
        String room_id = scanner.nextLine();
        try {
            establishConnection();
            Statement removeRoomStatement = connection.createStatement();
            Statement removeRoomStatement1 = connection.createStatement();

            String checkQuery = "SELECT room_id FROM room";
            ResultSet roomIds = removeRoomStatement1.executeQuery(checkQuery);
            boolean isRoomValid = false;
            while (roomIds.next()) {
                if (roomIds.getInt("room_id") == Integer.parseInt(room_id)) {
                    isRoomValid = true;
                }
            }
            if (isRoomValid) {
                String removeRoomQuery = "DELETE FROM RoomReservation.Room WHERE(" + "room_id = '" + room_id + "')";
                removeRoomStatement.executeUpdate(removeRoomQuery);
                System.out.println("-> Room removed successfully.");
            } else System.out.println("-> Please enter a existing room id to remove.");

            closeConnection();
        } catch (Exception e) {
            System.out.println("-> " + "Please enter proper input.");
        }
    }

    public static void listWithVacancy() {
        System.out.print("-> " + "Please enter the building id to list all rooms in it." + "\n" + "building_id >: ");
        String building_id = scanner.nextLine();

        try {
            establishConnection();
            Statement listWithVacanyStatement = connection.createStatement();
            Statement listWithVacanyStatement1 = connection.createStatement();
            int count = 0;

            String listWithVacanyReservedQuery = "Select ro.room_id, ro.room_name, b.building_name, ro.capacity FROM room as ro, buildings as b, reservation as re " +
                    "WHERE re.room_id = ro.room_id and ro.building_id = b.building_id and re.is_active = true and b.building_id = " + building_id;
            ResultSet listWithVacanyReserved = listWithVacanyStatement.executeQuery(listWithVacanyReservedQuery);

            String listWithVacanyNotReservedQuery = "Select DISTINCT ro.room_id, ro.room_name, b.building_name, ro.capacity " +
                    "FROM room as ro, buildings as b " +
                    "WHERE ro.building_id = b.building_id and b.building_id = " + building_id + " and " +
                    "(ro.room_id, ro.room_name, b.building_name, ro.capacity)NOT IN(Select ro.room_id, ro.room_name, b.building_name, ro.capacity FROM room as ro, buildings as b, reservation as re " +
                    "WHERE re.room_id = ro.room_id and ro.building_id = b.building_id and re.is_active = true and b.building_id = " + building_id + ")";
            ResultSet listWithVacanyNotReserved = listWithVacanyStatement1.executeQuery(listWithVacanyNotReservedQuery);

            while (listWithVacanyReserved.next()) {
                count++;
                System.out.println("-> " + "room id: " + listWithVacanyReserved.getInt("room_id") +
                        ", room name: " + listWithVacanyReserved.getString("room_name") +
                        ", building name: " + listWithVacanyReserved.getString("building_name") +
                        ", capacity: " + listWithVacanyReserved.getInt("capacity") +
                        ", Currently reserved: Yes");
            }
            while (listWithVacanyNotReserved.next()) {
                count++;
                System.out.println("-> " + "room id: " + listWithVacanyNotReserved.getInt("room_id") +
                        ", room name: " + listWithVacanyNotReserved.getString("room_name") +
                        ", building name: " + listWithVacanyNotReserved.getString("building_name") +
                        ", capacity: " + listWithVacanyNotReserved.getInt("capacity") +
                        ", Currently reserved: No");
            }
            if(count == 0) System.out.println("-> There is no room to show in this building.");

        closeConnection();
        } catch (Exception e) {

        }
    }

    public static void getOccupiedRooms() {
        try {
            establishConnection();
            Statement getOccupiedRoomsStatement = connection.createStatement();
            String getOccupiedRoomsQuery = "SELECT Ro.room_id, Ro.room_name, Ro.building_id, Ro.capacity, Ro.property_string, B.building_name " +
                    "FROM Room Ro, Reservation Re, Buildings B  WHERE Ro.room_id = Re.room_id and Ro.building_id = B.building_id and Re.is_active = true";
            ResultSet getOccupiedRoomsResult = getOccupiedRoomsStatement.executeQuery(getOccupiedRoomsQuery);

            int loopCount = 0;
            while (getOccupiedRoomsResult.next()) {
                loopCount++;
                if (loopCount == 1) {
                    System.out.println("Occupied rooms are:");
                }
                System.out.println("-> " + "Room Id: " + getOccupiedRoomsResult.getInt("room_id") +
                        ", Room Name: " + getOccupiedRoomsResult.getString("room_name") +
                        ", Building Name: " + getOccupiedRoomsResult.getString("building_name") +
                        ", Capacity: " + getOccupiedRoomsResult.getInt("capacity") +
                        ", Property: " + getOccupiedRoomsResult.getString("property_string"));
            }
            if(loopCount == 0){
                System.out.println("-> All rooms are available now.");
            }
            closeConnection();
        } catch (Exception e) {
            System.out.println("-> Please enter a proper query");
        }
    }

    public static void searchForRoom() {
        System.out.print("-> Please enter a query to search for a room" + "\n" + "Search Keyword >: ");
        String property = scanner.nextLine();
        try {
            establishConnection();
            Statement searchForRoomStatement = connection.createStatement();
            Statement searchForRoomStatement1 = connection.createStatement();

            String searchForRoomQueryWithReservation = "(SELECT Ro.room_id, Ro.room_name, B.building_name, Ro.capacity FROM room as ro, buildings b, reservation re " +
                    "WHERE ro.property_string = '" + property + "' and ro.building_id = b.building_id and re.room_id = ro.room_id and re.is_active = true)";

            ResultSet searchResultReserved = searchForRoomStatement.executeQuery(searchForRoomQueryWithReservation);

            String searchForRoomQueryWithNoReservation = "SELECT DISTINCT Ro.room_id, Ro.room_name, B.building_name, Ro.capacity FROM room as ro, buildings b   " +
                    "WHERE ro.property_string = '" + property + "' and ro.building_id = b.building_id and" +
                    " (Ro.room_id, Ro.room_name, B.building_name, Ro.capacity) NOT IN(SELECT Ro.room_id, Ro.room_name, B.building_name, Ro.capacity " +
                    "FROM room as ro, buildings b, reservation re WHERE ro.property_string = '" + property + "' and ro.building_id = b.building_id and re.room_id = ro.room_id and re.is_active = true)";

            ResultSet searchResultNotReserved = searchForRoomStatement1.executeQuery(searchForRoomQueryWithNoReservation);
            int count = 0;
            while (searchResultReserved.next()) {
                count++;
                System.out.println("-> " + "room id: " + searchResultReserved.getInt("room_id") +
                        ", room name: " + searchResultReserved.getString("room_name") +
                        ", building name: " + searchResultReserved.getString("building_name") +
                        ", capacity: " + searchResultReserved.getInt("capacity") +
                        ", Currently reserved: Yes");
            }
            while (searchResultNotReserved.next()) {
                count++;
                System.out.println("-> " + "room id: " + searchResultNotReserved.getInt("room_id") +
                        ", room name: " + searchResultNotReserved.getString("room_name") +
                        ", building name: " + searchResultNotReserved.getString("building_name") +
                        ", capacity: " + searchResultNotReserved.getInt("capacity") +
                        ", Currently reserved: No");
            }
            if (count == 0) {
                System.out.println("-> There is no room with searched property.");
            }
            closeConnection();
        } catch (Exception e) {
            System.out.println("-> Please enter a proper property string to list the rooms.");
        }
    }

    public static void getReservationHistoryOfRoom() {
        System.out.print("-> " + "Please enter Room ID to see the reservation history of the room" + "\n" + "room_id >: ");
        String room_id = scanner.nextLine();

        try {
            establishConnection();
            Statement getHistoryOfaRoomStatement = connection.createStatement();
            Statement roomIdStatement = connection.createStatement();

            String getRoomIdsQuery = "SELECT room_id FROM room";
            ResultSet roomIds = roomIdStatement.executeQuery(getRoomIdsQuery);
            boolean isRoomValid = false;
            while (roomIds.next()) {
                if (roomIds.getInt("room_id") == Integer.parseInt(room_id)) {
                    isRoomValid = true;
                }
            }
            int count = 0;
            if (isRoomValid) {
                String getHistoryOfaRoomQuery = "SELECT Ro.room_name, Re.is_active, i.instructor_name FROM Reservation as Re, Room as Ro, Instructor as i " +
                        "WHERE Re.room_id =" + room_id + " and Re.room_id = Ro.room_id and i.instructor_id = Re.instructor_id";
                ResultSet historyOfaRoomResult = getHistoryOfaRoomStatement.executeQuery(getHistoryOfaRoomQuery);
                while (historyOfaRoomResult.next()) {
                    count++;
                    if(count == 1) System.out.println("The room with room id: " + room_id + " has the following reservations:");
                    String isActive = "";
                    if (historyOfaRoomResult.getBoolean("is_active")) {
                        isActive = "Active";
                    } else {
                        isActive = "Not Active";
                    }
                    System.out.println("-> " + "Room Name: " + historyOfaRoomResult.getString("room_name") + ", Instructor Name: " +
                            historyOfaRoomResult.getString("instructor_name") + ", Reservation Situation: " + isActive);
                }
            }if(count == 0) System.out.println("-> The room has no reservation history.");
            closeConnection();
        } catch (Exception e) {

        }
    }

    public static void getReservationHistoryOfInstructor() {
        System.out.print("-> " + "Please enter Instructor ID to see the reservation history of the instructor" + "\n" + "instructor_id >: ");
        String instructor_id = scanner.nextLine();

        try {
            establishConnection();
            Statement getHistoryOfanInstructor = connection.createStatement();
            Statement instructorIdStatement = connection.createStatement();

            String getInstructorNamesQuery = "SELECT * FROM instructor";
            ResultSet instructorIds = instructorIdStatement.executeQuery(getInstructorNamesQuery);
            boolean isInstructorValid = false;
            while (instructorIds.next()) {
                if (instructorIds.getInt("instructor_id") == Integer.parseInt(instructor_id)) {
                    isInstructorValid = true;
                }
            }
            int count = 0;
            if (isInstructorValid) {
                String getHistoryOfanInstructorQuery = "SELECT Ro.room_name, Re.is_active, i.instructor_name FROM Reservation as Re, Room as Ro, Instructor as i " +
                        "WHERE Re.instructor_id =" + instructor_id + " and Re.room_id = Ro.room_id and i.instructor_id = Re.instructor_id";
                ResultSet getHistoryOfanInstructorResult = getHistoryOfanInstructor.executeQuery(getHistoryOfanInstructorQuery);
                while (getHistoryOfanInstructorResult.next()) {
                    if(count == 1) System.out.println("The instructor with instructor_id: " + instructor_id + " has the following reservations:");
                    String isActive = "";
                    if (getHistoryOfanInstructorResult.getBoolean("is_active")) {
                        isActive = "Active";
                    } else {
                        isActive = "Not Active";
                    }
                    //I have added one more field to show which is the situation of the reservation.
                    System.out.println("-> " + "Instructor Name: " + getHistoryOfanInstructorResult.getString("instructor_name") +
                            ", Room Name: " + getHistoryOfanInstructorResult.getString("room_name") + ", Reservation Situation: " + isActive);
                }
            }if(count == 0) System.out.println("-> The instructor has no reservation history.");
            closeConnection();
        } catch (Exception e) {

        }
    }

    public static void reserveRoom() {
        System.out.print("-> " + "Please enter Instructor ID and Room ID to reserve a room" + "\n" + "instructor_id >: ");
        String instructor_id = scanner.nextLine();
        System.out.print("room_id >: ");
        String room_id = scanner.nextLine();
        try {
            establishConnection();
            Statement reserveRoomStatement1 = connection.createStatement();
            Statement reserveRoomStatement2 = connection.createStatement();
            Statement reserveRoomStatement3 = connection.createStatement();

            String getRoomIds = "SELECT room_id FROM room";
            ResultSet roomId = reserveRoomStatement1.executeQuery(getRoomIds);
            ArrayList<Integer> roomIds = new ArrayList();
            boolean isValidRoom = false;

            String getInstructorIds = "SELECT instructor_id FROM instructor";
            ResultSet instructorId = reserveRoomStatement2.executeQuery(getInstructorIds);
            ArrayList<Integer> instructorIds = new ArrayList();
            boolean isValidInstructor = false;

            String getReservationsQuery = "SELECT room_id, is_active FROM Reservation WHERE is_active = true";
            ResultSet Reservations = reserveRoomStatement3.executeQuery(getReservationsQuery);
            boolean isValidReservation = true;

            while (roomId.next()) {
                roomIds.add(roomId.getInt("room_id"));
            }
            while (instructorId.next()) {
                instructorIds.add(instructorId.getInt("instructor_id"));
            }
            while (Reservations.next()) {
                if (Reservations.getInt("room_id") == Integer.parseInt(room_id)) {
                    isValidReservation = false;
                }
            }

            for (int element1 = 0; element1 < roomIds.size(); element1++) {
                if (roomIds.get(element1) == Integer.parseInt(room_id)) {
                    isValidRoom = true;
                }
            }
            for (int element2 = 0; element2 < instructorIds.size(); element2++) {
                if (instructorIds.get(element2) == Integer.parseInt(instructor_id)) {
                    isValidInstructor = true;
                }
            }
            if (isValidRoom && isValidInstructor && isValidReservation) {
                String reserveRoomQuery = "INSERT INTO RoomReservation.Reservation(ROOM_ID, INSTRUCTOR_ID, IS_ACTIVE) VALUES('" + room_id + "','" + instructor_id + "'," + true + ")";
                reserveRoomStatement1.executeUpdate(reserveRoomQuery);
                System.out.println("-> Room is reserved successfully.");
            } else if (isValidRoom && !isValidInstructor) {
                System.out.println("-> There is no such instructor id please enter proper one.");
            } else if (!isValidRoom && isValidInstructor) {
                System.out.println("-> There is no such room id please enter proper one.");
            } else if (!isValidReservation) {
                System.out.println("-> That room is reserved you can't reserve it please select another one.");
            } else {
                System.out.println("-> Please enter a proper instructor id and room id.");
            }

            closeConnection();
        } catch (Exception e) {
        }
    }

    public static void leaveRoom() {
        System.out.print("-> " + "Please enter the Room ID that you want to leave" + "\n" + "room_id >: ");
        String room_id = scanner.nextLine();
        try {
            establishConnection();
            Statement leaveRoomStatement = connection.createStatement();
            String getRoomIds = "SELECT room_id FROM Reservation";
            ResultSet roomId = leaveRoomStatement.executeQuery(getRoomIds);
            boolean isValid = false;
            ArrayList<Integer> roomIds = new ArrayList();
            while (roomId.next()) {
                roomIds.add(roomId.getInt("room_id"));
            }
            for (int element = 0; element < roomIds.size(); element++) {
                if (roomIds.get(element) == Integer.parseInt(room_id)) {
                    isValid = true;
                }
            }
            if (isValid) {
                String leaveRoomQuery = "UPDATE reservation as re SET is_active = false WHERE re.room_id = " + "'" + room_id + "'";
                leaveRoomStatement.executeUpdate(leaveRoomQuery);
                System.out.println("-> You left the room successfully.");
            } else System.out.println("-> There is no such room id reserved please enter proper one.");

            closeConnection();
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        openingScreen(menuHolderArray);
        navigation(menuHolderArray);
    }
}
