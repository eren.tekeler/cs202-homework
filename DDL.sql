CREATE DATABASE  IF NOT EXISTS RoomReservation;
CREATE TABLE IF NOT EXISTS RoomReservation.Instructor(
instructor_id int AUTO_INCREMENT PRIMARY KEY,
instructor_name VARCHAR(30) NOT NULL,
instructor_email VARCHAR(30) NOT NULL,
instructor_origin VARCHAR(30) NOT NULL
);

CREATE TABLE IF NOT EXISTS RoomReservation.Buildings(
building_id int AUTO_INCREMENT PRIMARY KEY,
building_name VARCHAR(30) NOT NULL
);

CREATE TABLE IF NOT EXISTS RoomReservation.Room(
room_id int AUTO_INCREMENT,
room_name VARCHAR(30) NOT NULL,
building_id int NOT NULL,
capacity int NOT NULL,
property_string VARCHAR(30),
PRIMARY KEY(room_id, room_name, building_id),
FOREIGN KEY (building_id) REFERENCES Buildings(building_id)
);

CREATE TABLE IF NOT EXISTS RoomReservation.Reservation(
reservation_id int AUTO_INCREMENT PRIMARY KEY,
room_id int NOT NULL,
instructor_id int NOT NULL,
is_active BOOLEAN NOT NULL,
FOREIGN KEY (room_id) REFERENCES Room(room_id),
FOREIGN KEY (instructor_id) REFERENCES Instructor(instructor_id)
);


