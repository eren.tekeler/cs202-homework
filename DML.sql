INSERT INTO RoomReservation.Instructor(instructor_name, instructor_email, instructor_origin)
VALUES
		('Muhammed Lee', 'muhammed@lee.com' , 'Istanbul/Turkey'),
        ('Chris Kon', 'chriscon@fakemail.com', 'New York/USA'),
        ('Mace Windu', 'macewindu@jedi.com', 'Coruscant/Jedi');
        
        
INSERT INTO RoomReservation.Buildings(building_name)
VALUES
		('Engineering'),
        ('Main Building'),
        ('Library'),
        ('Business'),
        ('Architecture');
        
        
INSERT INTO RoomReservation.room(room_name, building_id , capacity, property_string)
VALUES
		('Study Room', 1, 1,'air conditioning'),
        ('Medical Room', 2, 1, 'whiteboard'),
        ('Math Room', 3, 1, 'projector'),
        ('Physics Room', 4, 1, 'whiteboard'),
        ('Chemistry Room', 5, 1, 'air conditioning');
        